class CreateJobStatusHistories < ActiveRecord::Migration
  def change
    create_table :job_status_histories do |t|
      t.integer :job_id
      t.integer :job_status_id
      t.datetime :job_status_history_date
      t.datetime :job_status_history_notes

      t.timestamps
    end
  end
end
