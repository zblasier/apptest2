class ChangeSalesAgentPhoneToStringInSalesAgent < ActiveRecord::Migration
  def up
    change_column :sales_agents, :sales_agent_phone, :string
  end

  def down
    change_column :sales_agents, :sales_agent_phone, :integer
  end
end
