class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :customer_id
      t.integer :sales_agent_id
      t.integer :job_source_id
      t.integer :job_type_id
      t.string :internal_job_id
      t.string :customer_home_address
      t.string :customer_home_city
      t.string :customer_home_zip
      t.string :customer_home_state
      t.decimal :job_price
      t.string :job_notes

      t.timestamps
    end
  end
end
