# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150322165646) do

  create_table "customer_statuses", force: true do |t|
    t.string   "customer_status_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "customer_first_name"
    t.string   "customer_last_name"
    t.string   "customer_home_address"
    t.string   "customer_home_state"
    t.string   "customer_home_zip"
    t.string   "customer_home_city"
    t.string   "customer_billing_address"
    t.string   "customer_billing_zip"
    t.string   "customer_billing_city"
    t.string   "customer_billing_state"
    t.string   "customer_primary_email"
    t.string   "customer_secondary_email"
    t.string   "customer_primary_phone"
    t.string   "customer_secondary_phone"
    t.string   "customer_notes"
    t.integer  "customer_status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "job_sources", force: true do |t|
    t.string   "job_source_name"
    t.string   "job_source_notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "job_status_histories", force: true do |t|
    t.integer  "job_id"
    t.integer  "job_status_id"
    t.datetime "job_status_history_date"
    t.datetime "job_status_history_notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "job_statuses", force: true do |t|
    t.string   "job_status_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "job_types", force: true do |t|
    t.string   "job_type_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.integer  "customer_id"
    t.integer  "sales_agent_id"
    t.integer  "job_source_id"
    t.integer  "job_type_id"
    t.string   "internal_job_id"
    t.string   "customer_home_address"
    t.string   "customer_home_city"
    t.string   "customer_home_zip"
    t.string   "customer_home_state"
    t.decimal  "job_price"
    t.string   "job_notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "job_status_id"
  end

  create_table "sales_agent_statuses", force: true do |t|
    t.string   "sales_agent_status_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales_agents", force: true do |t|
    t.string   "sales_agent_first_name"
    t.string   "sales_agent_last_name"
    t.string   "sales_agent_phone"
    t.decimal  "sales_agent_comission"
    t.string   "sales_agent_email"
    t.integer  "sales_agent_status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
