require 'test_helper'

class JobStatusLinesControllerTest < ActionController::TestCase
  setup do
    @job_status_line = job_status_lines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:job_status_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create job_status_line" do
    assert_difference('JobStatusHistory.count') do
      post :create, job_status_line: { job_status_date: @job_status_line.job_status_date, job_status_id: @job_status_line.job_status_id }
    end

    assert_redirected_to job_status_line_path(assigns(:job_status_line))
  end

  test "should show job_status_line" do
    get :show, id: @job_status_line
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @job_status_line
    assert_response :success
  end

  test "should update job_status_line" do
    patch :update, id: @job_status_line, job_status_line: { job_status_date: @job_status_line.job_status_date, job_status_id: @job_status_line.job_status_id }
    assert_redirected_to job_status_line_path(assigns(:job_status_line))
  end

  test "should destroy job_status_line" do
    assert_difference('JobStatusHistory.count', -1) do
      delete :destroy, id: @job_status_line
    end

    assert_redirected_to job_status_lines_path
  end
end
