require 'test_helper'

class CustomersControllerTest < ActionController::TestCase
  setup do
    @customer = customers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post :create, customer: { customer_billing_address: @customer.customer_billing_address, customer_billing_city: @customer.customer_billing_city, customer_billing_zip: @customer.customer_billing_zip, customer_first_name: @customer.customer_first_name, customer_home_address: @customer.customer_home_address, customer_home_city: @customer.customer_home_city, customer_home_zip: @customer.customer_home_zip, customer_last_name: @customer.customer_last_name, customer_primary_email: @customer.customer_primary_email, customer_status_id: @customer.customer_status_id }
    end

    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should show customer" do
    get :show, id: @customer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer
    assert_response :success
  end

  test "should update customer" do
    patch :update, id: @customer, customer: { customer_billing_address: @customer.customer_billing_address, customer_billing_city: @customer.customer_billing_city, customer_billing_zip: @customer.customer_billing_zip, customer_first_name: @customer.customer_first_name, customer_home_address: @customer.customer_home_address, customer_home_city: @customer.customer_home_city, customer_home_zip: @customer.customer_home_zip, customer_last_name: @customer.customer_last_name, customer_primary_email: @customer.customer_primary_email, customer_status_id: @customer.customer_status_id }
    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete :destroy, id: @customer
    end

    assert_redirected_to customers_path
  end
end
