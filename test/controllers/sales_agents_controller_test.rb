require 'test_helper'

class SalesAgentsControllerTest < ActionController::TestCase
  setup do
    @sales_agent = sales_agents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_agents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_agent" do
    assert_difference('SalesAgent.count') do
      post :create, sales_agent: { sales_agent_comission: @sales_agent.sales_agent_comission, sales_agent_email: @sales_agent.sales_agent_email, sales_agent_first_name: @sales_agent.sales_agent_first_name, sales_agent_last_name: @sales_agent.sales_agent_last_name, sales_agent_phone: @sales_agent.sales_agent_phone, sales_agent_status_id: @sales_agent.sales_agent_status_id }
    end

    assert_redirected_to sales_agent_path(assigns(:sales_agent))
  end

  test "should show sales_agent" do
    get :show, id: @sales_agent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_agent
    assert_response :success
  end

  test "should update sales_agent" do
    patch :update, id: @sales_agent, sales_agent: { sales_agent_comission: @sales_agent.sales_agent_comission, sales_agent_email: @sales_agent.sales_agent_email, sales_agent_first_name: @sales_agent.sales_agent_first_name, sales_agent_last_name: @sales_agent.sales_agent_last_name, sales_agent_phone: @sales_agent.sales_agent_phone, sales_agent_status_id: @sales_agent.sales_agent_status_id }
    assert_redirected_to sales_agent_path(assigns(:sales_agent))
  end

  test "should destroy sales_agent" do
    assert_difference('SalesAgent.count', -1) do
      delete :destroy, id: @sales_agent
    end

    assert_redirected_to sales_agents_path
  end
end
