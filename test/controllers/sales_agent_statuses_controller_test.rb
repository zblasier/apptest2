require 'test_helper'

class SalesAgentStatusesControllerTest < ActionController::TestCase
  setup do
    @sales_agent_status = sales_agent_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_agent_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_agent_status" do
    assert_difference('SalesAgentStatus.count') do
      post :create, sales_agent_status: { sale_agent_status_date: @sales_agent_status.sale_agent_status_date, sale_agent_status_name: @sales_agent_status.sale_agent_status_name }
    end

    assert_redirected_to sales_agent_status_path(assigns(:sales_agent_status))
  end

  test "should show sales_agent_status" do
    get :show, id: @sales_agent_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_agent_status
    assert_response :success
  end

  test "should update sales_agent_status" do
    patch :update, id: @sales_agent_status, sales_agent_status: { sale_agent_status_date: @sales_agent_status.sale_agent_status_date, sale_agent_status_name: @sales_agent_status.sale_agent_status_name }
    assert_redirected_to sales_agent_status_path(assigns(:sales_agent_status))
  end

  test "should destroy sales_agent_status" do
    assert_difference('SalesAgentStatus.count', -1) do
      delete :destroy, id: @sales_agent_status
    end

    assert_redirected_to sales_agent_statuses_path
  end
end
