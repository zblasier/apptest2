class JobStatusHistoriesController < ApplicationController
  before_action :set_job_status_history, only: [:show, :edit, :update, :destroy]

  # GET /job_status_histories
  # GET /job_status_histories.json
  def index
    @job_status_histories = JobStatusHistory.all
  end

  # GET /job_status_histories/1
  # GET /job_status_histories/1.json
  def show
  end

  # GET /job_status_histories/new
  def new
    @job_status_history = JobStatusHistory.new
  end

  # GET /job_status_histories/1/edit
  def edit
  end

  # POST /job_status_histories
  # POST /job_status_histories.json
  def create
    @job_status_history = JobStatusHistory.new(job_status_history_params)

    respond_to do |format|
      if @job_status_history.save
        format.html { redirect_to @job_status_history, notice: 'Job status history was successfully created.' }
        format.json { render action: 'show', status: :created, location: @job_status_history }
      else
        format.html { render action: 'new' }
        format.json { render json: @job_status_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_status_histories/1
  # PATCH/PUT /job_status_histories/1.json
  def update
    respond_to do |format|
      if @job_status_history.update(job_status_history_params)
        format.html { redirect_to @job_status_history, notice: 'Job status history was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @job_status_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_status_histories/1
  # DELETE /job_status_histories/1.json
  def destroy
    @job_status_history.destroy
    respond_to do |format|
      format.html { redirect_to job_status_histories_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_status_history
      @job_status_history = JobStatusHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_status_history_params
      params.require(:job_status_history).permit(:job_status_id, :job_status_history_date, :job_id, :job_status_history_notes)
    end
end
