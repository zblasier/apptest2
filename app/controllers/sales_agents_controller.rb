class SalesAgentsController < ApplicationController
  before_action :set_sales_agent, only: [:show, :edit, :update, :destroy]

  # GET /sales_agents
  # GET /sales_agents.json
  def index
    @sales_agents = SalesAgent.all
  end

  # GET /sales_agents/1
  # GET /sales_agents/1.json
  def show
  end

  def search
    @sales_agents = SalesAgent.search params[:search],params[:criteria]
  end

  # GET /sales_agents/new
  def new
    @sales_agent = SalesAgent.new
  end

  # GET /sales_agents/1/edit
  def edit
  end

  # POST /sales_agents
  # POST /sales_agents.json
  def create
    @sales_agent = SalesAgent.new(sales_agent_params)

    respond_to do |format|
      if @sales_agent.save
        format.html { redirect_to @sales_agent, notice: 'Sales agent was successfully created.' }
        format.json { render action: 'show', status: :created, location: @sales_agent }
      else
        format.html { render action: 'new' }
        format.json { render json: @sales_agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_agents/1
  # PATCH/PUT /sales_agents/1.json
  def update
    respond_to do |format|
      if @sales_agent.update(sales_agent_params)
        format.html { redirect_to @sales_agent, notice: 'Sales agent was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sales_agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_agents/1
  # DELETE /sales_agents/1.json
  def destroy
    @sales_agent.destroy
    respond_to do |format|
      format.html { redirect_to sales_agents_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_agent
      @sales_agent = SalesAgent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_agent_params
      params.require(:sales_agent).permit(:sales_agent_first_name, :sales_agent_last_name, :sales_agent_phone, :sales_agent_comission, :sales_agent_email, :sales_agent_status_id)
    end
end
