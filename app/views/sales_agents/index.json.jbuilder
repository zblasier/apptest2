json.array!(@sales_agents) do |sales_agent|
  json.extract! sales_agent, :id, :sales_agent_first_name, :sales_agent_last_name, :sales_agent_phone, :sales_agent_comission, :sales_agent_email, :sales_agent_status_id
  json.url sales_agent_url(sales_agent, format: :json)
end
