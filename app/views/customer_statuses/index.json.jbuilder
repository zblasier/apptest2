json.array!(@customer_statuses) do |customer_status|
  json.extract! customer_status, :id, :customer_status_name
  json.url customer_status_url(customer_status, format: :json)
end
