class JobStatus < ActiveRecord::Base
  belongs_to :job
  has_many :job_status_histories
  has_many :jobs, through: :job_status_histories
  validates_uniqueness_of :job_status_name
end
