class JobType < ActiveRecord::Base
  has_many :jobs
  validates_uniqueness_of :job_type_name
end
