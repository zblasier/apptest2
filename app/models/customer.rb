class Customer < ActiveRecord::Base
  has_many :jobs
  belongs_to :customer_status
  validates :customer_last_name, :customer_first_name, :customer_status_id, presence: true
  validates :customer_primary_phone || :customer_secondary_phone, length: { minimum: 10 }, :unless => Proc.new { |a|  a.customer_primary_phone.nil? || a.customer_secondary_phone.nil? & a.customer_primary_email? }
  validates :customer_primary_email || :customer_secondary_email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, }, :unless => Proc.new { |a| a.customer_primary_email.nil? || a.customer_secondary_email.nil? & a.customer_primary_phone? }
  validates_uniqueness_of :customer_primary_email, :customer_primary_phone, :scope => [:customer_first_name, :customer_last_name], :message => 'User already exists. You may check inactive users.'

  before_validation{|record| if record.customer_primary_phone; record.customer_primary_phone.gsub!(/\D/, "") end; if record.customer_secondary_phone; record.customer_secondary_phone.gsub!(/\D/, "") end}



  def name_with_initial
    "#{customer_first_name.first}. #{customer_last_name}"
  end

  def full_name
    "#{customer_first_name} #{customer_last_name}"
  end

  def self.search(search, criteria)
    if criteria
    find(:all, :conditions => ["#{criteria[0]} LIKE ?", search+'%'], :limit => 10)
    else
      []
    end
  end

end
